import HerancaPolimorfismo.Terceiro.Cachorro;
import HerancaPolimorfismo.Terceiro.Gato;
        



public class Teste2 {
    public static void main(String[] args) {
        
        Gato gato = new Gato() ; 
        Cachorro cachorro = new Cachorro() ; 
        gato.mia(); 
        cachorro.latir();
        gato.caminha();
        cachorro.caminha();
        
        
        
        
    }
}
