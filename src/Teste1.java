import HerancaPolimorfismo.segundo.Administrativo;
import HerancaPolimorfismo.segundo.Assistente;
import HerancaPolimorfismo.segundo.Tecnico;

public class Teste1 {

   public static void main(String[] args) {
        
        Assistente adm = new Administrativo("5464556", "Noite") ; 
        adm.setNome("Daniel");
        Assistente tec = new Tecnico("4234234") ; 
        tec.setNome("Jose");
        
        System.out.println(adm);
        System.out.println(tec);
        
        
        

    }
}
