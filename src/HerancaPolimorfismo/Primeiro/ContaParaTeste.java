
package HerancaPolimorfismo.Primeiro;


public class ContaParaTeste {

    public static void main(String[] args) {
        Conta conta1 = new ContaCorrente(10, 100);
        Conta conta2 = new ContaPoupanca();

        conta1.depositar(100);
        conta1.sacar(20);
        conta1.saldoImpresso();
        
        
        
    }

}