
package HerancaPolimorfismo.Primeiro;

import java.text.NumberFormat;


public abstract class Conta {

    protected double saldo;
    protected double taxa;

    public Conta(double taxa) {
        this.taxa = taxa;
    }

    public abstract void sacar(double valor);

    public void depositar(double valor) {
        this.saldo += valor;
    }

    public double getSaldo() {
        return saldo;
    }

    public void saldoImpresso() {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        System.out.println(nf.format(this.getSaldo()));

    }

}