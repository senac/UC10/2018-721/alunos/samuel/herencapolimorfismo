
package HerancaPolimorfismo.Quarto;

public abstract class Ingresso {

    protected double valor;

    public Ingresso(double valor) {
        this.valor = valor;
    }

    public void imprimeValor() {
        System.out.println(StringUtils.formatarMonetario(valor));
    }

}
