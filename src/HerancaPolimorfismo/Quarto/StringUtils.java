
package HerancaPolimorfismo.Quarto;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class StringUtils {

    public static final String FORMATO_DATA = "dd/MM/yy";
    public static final String FORMATO_DATA_HORA = "dd/MM/yy HH:mm:ss";

    public StringUtils() {

    }

    public static int contarCaracteres(String palavra) {
        return palavra.length();
    }

    public static String formatarMonetario(double valor) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        return nf.format(valor);
    }

    public static String formatarData(Date data) {
        DateFormat df = new SimpleDateFormat(FORMATO_DATA);
        return df.format(data);
    }

    public static String formatarDataComTempo(Date data) {
        DateFormat df = new SimpleDateFormat(FORMATO_DATA_HORA);
        return df.format(data);
    }
    

}