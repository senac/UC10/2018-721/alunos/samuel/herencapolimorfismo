
package HerancaPolimorfismo.Quarto;

public  class Vip extends Ingresso {

    private final double adicional = 50;

    public Vip(double valor) {
        super(valor);
    }

    @Override
    public final void imprimeValor() {
        System.out.println(StringUtils.formatarMonetario(this.valor + this.adicional));
    }

}
