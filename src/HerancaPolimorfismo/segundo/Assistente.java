
package HerancaPolimorfismo.segundo;

import java.util.Objects;


public abstract class Assistente extends Funcionario {

    private String matricula;
    

    public Assistente(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.matricula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Assistente other = (Assistente) obj;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        return true;
    }

    
    

    @Override
    public String toString() {
        return "Nome:" + this.getNome() + " - Matricula:" + this.matricula ; 
    }

}
