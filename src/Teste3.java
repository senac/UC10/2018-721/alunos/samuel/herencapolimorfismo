

import HerancaPolimorfismo.Terceiro.Animal;
import HerancaPolimorfismo.Terceiro.Cachorro;
import HerancaPolimorfismo.Terceiro.Miseravel;
import HerancaPolimorfismo.Terceiro.Pessoa;
import HerancaPolimorfismo.Terceiro.Pobre;
import HerancaPolimorfismo.Terceiro.Rica;
import HerancaPolimorfismo.Terceiro.Comunicavel;
import HerancaPolimorfismo.Terceiro.Gato;

import java.util.ArrayList;
import java.util.List;


public class Teste3 {
  public static void main(String[] args) {

        Rica rica = new Rica();
        Pobre pobre = new Pobre();
        Miseravel miseravel = new Miseravel();
        Gato gato = new Gato();
        Cachorro cachorro = new Cachorro();

        List<Pessoa> pessoas = new ArrayList<>();

        List<Comunicavel> lista = new ArrayList<>();

        pessoas.add(pobre);
        pessoas.add(rica);
        pessoas.add(miseravel);

        lista.add(gato);
        lista.add(cachorro);
        lista.add(rica);
        lista.add(pobre);
        lista.add(miseravel);

        for (Comunicavel c : lista) {

            if (c instanceof Animal) {
                Animal a = (Animal) c;
                a.caminha();
            }

        }

    }  
}
